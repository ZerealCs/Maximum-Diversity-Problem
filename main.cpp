#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <chrono>

#include "solver/representation/Problem.h"
#include "solver/algorithms/GreedyBuilder.h"
#include "solver/algorithms/GreedyAlternative.h"
#include "solver/algorithms/GreedyDestructor.h"
#include "solver/algorithms/LocalSearch.h"
#include "solver/algorithms/BranchAndBound.h"
#include "solver/algorithms/Grasp.h"

using namespace std;


void exec(Problem problem) {
    cout << "Selecione el algoritmo con el que ejecutar" << endl
    << "1 → BuilderGreedy" << endl
    << "2 → Alternative Greedy" << endl
    << "3 → LocalSearch" << endl
    << "4 → GRASP" << endl
    << "5 → BranchAndBound" << endl;

    int option;
    cin >> option;
    Algorithm * algorithm;
    bool grasp = false;
    bool branch = false;
    switch (option) {
        case 1:
            algorithm = new GreedyBuilder();
            break;
        case 2:
            algorithm = new GreedyDestructor();
            break;
        case 3:
            algorithm = new LocalSearch();
            break;
        case 4:
            grasp = true;
            break;
        case 5:
            branch = true;
            break;
    }
    cout << "Variacion de m" << endl;
    int mIni;
    cin >> mIni;
    int mEnd;
    cin >> mEnd;
    for (int m = mIni; m < mEnd; ++m) {
        if (grasp) {
            for (int i = 0; i < 2; ++i) {
                algorithm = new Grasp(i+2);
                auto t_start = std::chrono::high_resolution_clock::now();
                Solution solution = algorithm->solve(problem, m);
                auto t_end = std::chrono::high_resolution_clock::now();
                double value = std::chrono::duration<double, std::milli>(t_end-t_start).count();
                cout << "| " <<problem.size() << " | "
                << problem.getKSize() << " | "
                << m << " | "
                << i + 2 << " | "
                << solution.cost() << " | "
                << value << " | "
                << solution << " |"<< endl;
            }
        } else if (branch) {
            algorithm = new BranchAndBound(false);
            auto t_start = std::chrono::high_resolution_clock::now();
            Solution solution = algorithm->solve(problem, m);
            auto t_end = std::chrono::high_resolution_clock::now();
            double value = std::chrono::duration<double, std::milli>(t_end - t_start).count();
            cout << "| " << problem.size() << " | "
            << problem.getKSize() << " | "
            << m << " | "
            << solution.cost() << " | "
            << value << " | "
            << ((BranchAndBound *)algorithm)->getNumNodes() << " | "
            << solution << " |" << endl;
        } else {
            auto t_start = std::chrono::high_resolution_clock::now();
            Solution solution = algorithm->solve(problem, m);
            auto t_end = std::chrono::high_resolution_clock::now();
            double value = std::chrono::duration<double, std::milli>(t_end - t_start).count();
            cout << "| " << problem.size() << " | "
            << problem.getKSize() << " | "
            << m << " | "
            << solution.cost() << " | "
            << value << " | "
            << solution << " |" << endl;
        }
    }

    delete algorithm;
}


int main(int argc, char const *argv[]) {
    string name = argv[0];
    if (argc == 2) {
        srand(time(NULL));
        fstream file(argv[1]);
        Problem problem = Problem(file);
        cout << problem << endl;
        exec(problem);
    }
    else {
        // Hacerlo random
        cout << "USAGE: " << name << " filename" << endl;
    }

    return 0;
}

