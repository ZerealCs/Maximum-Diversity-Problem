/*
 * =====================================================================================
 *
 *       Filename:  GreedyDestructor.h.h
 *
 *    Description:  TODO: comentar algo
 *
 *        Version:  1.0
 *        Created:  9/05/16
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */
//

#ifndef MAXIMUM_DIVERSITY_PROBLEM_GREEDYDESTRUCTOR_H
#define MAXIMUM_DIVERSITY_PROBLEM_GREEDYDESTRUCTOR_H

#include "../Algorithm.h"
#include "../util.h"

class GreedyDestructor : public Algorithm {
public:
    GreedyDestructor(void);

    /**
     * Solve algorithm applying a constructor greedy algorithm
     */
    virtual Solution solve(Problem problem, int size) override;

};


#endif //MAXIMUM_DIVERSITY_PROBLEM_GREEDYDESTRUCTOR_H
