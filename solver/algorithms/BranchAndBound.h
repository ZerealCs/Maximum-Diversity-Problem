/*
 * =====================================================================================
 *
 *       Filename:  BranchAndBound.h.h
 *
 *    Description:  TODO: comentar algo
 *
 *        Version:  1.0
 *        Created:  14/05/16
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */
//

#ifndef MAXIMUM_DIVERSITY_PROBLEM_BRANCHANDBOUND_H
#define MAXIMUM_DIVERSITY_PROBLEM_BRANCHANDBOUND_H

#include <list>
#include <vector>
#include <algorithm>
#include <utility>
#include "../Algorithm.h"
#include "GreedyBuilder.h"
#include "GreedyDestructor.h"
#include "Grasp.h"

class BranchAndBound : public Algorithm {
private:
    /**
     * Num of nodes explored
     */
    int numNodes;
    /**
     * Search in depth?
     */
    bool searchInDepth;

    /**
     * Auxiliar class
     */
    class Node {
    public:
        int k;
        int i;
        Node * last;
    public:
        Node(int i, int k, Node * last): i(i), k(k), last(last) {
        }

        Solution getSolution(vector<Point> p) {
            Solution result;
            Node * aux = this;
            do {
                result.insert(p[aux->i]);
                aux = aux->last;
            } while (aux != nullptr);
            return result;
        }

        int getK() const {
            return k;
        }

        void setK(int k) {
            Node::k = k;
        }

        int getI() const {
            return i;
        }

        void setI(int i) {
            Node::i = i;
        }

        Node *getLast() const {
            return last;
        }

        void setLast(Node *last) {
            Node::last = last;
        }
    };
public:
    /**
     * Let select two way of search in depth or best fixed max bound in each expasion
     */
    BranchAndBound(bool depth);

    virtual Solution solve(Problem problem, int size) override;

private:

    double getUB2(Solution sol, vector<Point> pro, int size) {
        vector<double> ub2(size - sol.size());
        for (auto point : sol) {
            for (auto it = pro.begin(); it != pro.end(); it++) {
                if (point == *it) {
                    pro.erase(it);
                }
            }
        }
        for (auto noSel: pro) {
            double acc = 0.0;
            for (auto sel : sol) {
                acc += noSel.distance(sel);
            }
            int minIx = min(ub2);
            if (ub2[minIx] < acc) {
                ub2[minIx] = acc;
            }
        }
        return sum(ub2);
    }

    double getUB3(Solution sol, vector<Point> pro, int size) {
        if (size - sol.size() == 1) {
            return 0;
        }
        vector<double> costTotal(size - sol.size());
        for (auto point : sol) {
            for (auto it = pro.begin(); it != pro.end(); it++) {
                if (point == *it) {
                    pro.erase(it);
                }
            }
        }
        for (int i = 0; i < pro.size() - 1; ++i) {
            vector<double> cost(size - sol.size() - 1);
            for (int j = i + 1; j < pro.size(); ++j) {
                int minIx = min(cost);
                double distance = pro[i].distance(pro[j]);
                if (cost[minIx] < distance) {
                    cost[minIx] = distance;
                }
            }
            double calcSum = 1/2.0 * sum(cost);

            int minIx = min(costTotal);
            if (costTotal[minIx] < calcSum) {
                costTotal[minIx] = calcSum;
            }
        }

        return sum(costTotal);
    }

public:
    int getNumNodes() const {
        return numNodes;
    }

};


#endif //MAXIMUM_DIVERSITY_PROBLEM_BRANCHANDBOUND_H
