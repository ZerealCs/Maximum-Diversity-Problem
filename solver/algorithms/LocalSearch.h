/*
 * =====================================================================================
 *
 *       Filename:  LocalSearch.h.h
 *
 *    Description:  TODO: comentar algo
 *
 *        Version:  1.0
 *        Created:  14/05/16
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */
//

#ifndef MAXIMUM_DIVERSITY_PROBLEM_LOCALSEARCH_H
#define MAXIMUM_DIVERSITY_PROBLEM_LOCALSEARCH_H

#include "../Algorithm.h"


class LocalSearch : public Algorithm {
public:
    LocalSearch(void);

    Solution solve(Problem p, int size) override;
};


#endif //MAXIMUM_DIVERSITY_PROBLEM_LOCALSEARCH_H
