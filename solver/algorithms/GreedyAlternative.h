/*
 * =====================================================================================
 *
 *       Filename:  GreedyAlternative.h.h
 *
 *    Description:  TODO: comentar algo
 *
 *        Version:  1.0
 *        Created:  16/05/16
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */
//

#ifndef MAXIMUM_DIVERSITY_PROBLEM_GREEDYALTERNATIVE_H
#define MAXIMUM_DIVERSITY_PROBLEM_GREEDYALTERNATIVE_H

#include "../Algorithm.h"
#include "../util.h"

class GreedyAlternative : public Algorithm {

public:
    GreedyAlternative() : Algorithm("GreedyAlternative") {
    }

    virtual Solution solve(Problem problem, int size) override {
        Solution solution(size);

        int remain = size;
        while (--remain >= 0) {
            double max = 0;
            Point maxPoint;
            for (auto point : problem) {
                solution.insert(point);
                double cost = solution.cost();
                if (cost >= max) {
                    max = cost;
                    maxPoint = point;
                }
                solution.erase(point);
            }
            solution.insert(maxPoint);
            problem.erase(maxPoint);
        }
        return solution;
    }
};


#endif //MAXIMUM_DIVERSITY_PROBLEM_GREEDYALTERNATIVE_H
