/*
 * =====================================================================================
 *
 *       Filename:  BranchAndBound.cpp.h
 *
 *    Description:  TODO: comentar algo
 *
 *        Version:  1.0
 *        Created:  14/05/16
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */
//

#include "BranchAndBound.h"


BranchAndBound::BranchAndBound(bool depth):
        Algorithm("BranchAndBound"),
        searchInDepth(depth),
        numNodes(0) {}

Solution BranchAndBound::solve(Problem problem, int size) {
    // Hacer un greedy o grasp
    // Solution lowerBound = GreedyBuilder().solve(problem, size);
    // Solution lowerBound = Grasp(2).solve(problem, size);
    Solution lowerBound = GreedyDestructor().solve(problem, size);

    // Nodos activos en el arbol
    vector<Point> problemVec;
    for (auto point : problem) {
        problemVec.push_back(point);
    }
    typedef Node* NodePtr;

    list<Node> activeNodes;

    auto cmpNodes = [&](Node a, Node b) {
        return a.getSolution(problemVec).cost() < b.getSolution(problemVec).cost();
    };

    auto getNodes = [&] (list<Node>::iterator it) {
        int i = (*it).getI() + 1;
        int k = (*it).getK();
        int n = problem.size();
        int m = size;
        int ini = i+1;
        int end = n - (m - k) + 1;

        if (k < m) {
            vector<Node> aux;
            auto itprime = it;
            if (itprime != activeNodes.end()) {
                itprime++;
            }
            for (int j = ini; j <= end; ++j) {
                auto node = Node(j - 1, k + 1, &(*it));
                aux.push_back(node);
                //activeNodes.insert(itprime, node);
            }
            if (!searchInDepth) {
                sort(aux.begin(), aux.end(), cmpNodes);
            }

            for (int l = 0; l < aux.size(); ++l) {
                activeNodes.insert(itprime, aux[l]);
            }
        }

    };

    for (int i = 1; i <= problem.size() - size + 1; ++i) {
        auto node = Node(i-1, 1, nullptr);
        activeNodes.push_back(node);
    }

    for (auto it = activeNodes.begin(); it != activeNodes.end(); it++) {

        Node take = (*it);

        Solution z = take.getSolution(problemVec);
        if (z.size() == size) {  // Solucion Completa

            if (z.cost() > lowerBound.cost()) {
                lowerBound = z;
            }
        }
        else { // Es una solucion parcial
            double ub23 = getUB2(z, problemVec, size) + getUB3(z, problemVec, size);

            if (z.cost() + ub23 > lowerBound.cost()) {
                getNodes(it);
            }
        }
    }

    numNodes = activeNodes.size();

    return lowerBound;
}

