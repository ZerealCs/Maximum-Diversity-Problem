/*
 * =====================================================================================
 *
 *       Filename:  GreedyBuilder.h
 *
 *    Description:  A constructor greedy
 *
 *        Version:  1.0
 *        Created:  9/05/16
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */
//

#ifndef MAXIMUM_DIVERSITY_PROBLEM_GREEDYBUILDER_H
#define MAXIMUM_DIVERSITY_PROBLEM_GREEDYBUILDER_H

#include <string>

#include "../Algorithm.h"
#include "../util.h"

using namespace std;

/**
 * A constructor greedy using the following algorithm
 * Elem = Problem's elements
 * S = ø
 * Get s_c = centro(Elem)
 * repeat
 *   get s_* in Elem more far from s_c
 *   S = S union {s_*}
 *   Elem = Elem - {s_*}
 *   Get S_c = centro(S)
 * until |S| = m
 * return S
 */
class GreedyBuilder : public Algorithm {
public:
    GreedyBuilder(void);

    /**
     * Solve algorithm applying a constructor greedy algorithm
     */
    virtual Solution solve(Problem problem, int size) override;
};


#endif //MAXIMUM_DIVERSITY_PROBLEM_GREEDYBUILDER_H
