/*
 * =====================================================================================
 *
 *       Filename:  GreedyDestructor.cpp.h
 *
 *    Description:  TODO: comentar algo
 *
 *        Version:  1.0
 *        Created:  9/05/16
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */
//

#include "GreedyDestructor.h"


GreedyDestructor::GreedyDestructor(void): Algorithm("Greedy_Destructor") {

}

Solution GreedyDestructor::solve(Problem problem, int size) {
    auto center = getCenter(problem);

    int remain = problem.size() - size;
    while (--remain >= 0) {
        Point point = getNearestPoint(center, problem);
        problem.erase(point);
        center = getCenter(problem);
    }

    return problem.getSolution(problem.size() - size);
}

