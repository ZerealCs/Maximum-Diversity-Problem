/*
 * =====================================================================================
 *
 *       Filename:  Grasp.cpp.h
 *
 *    Description:  TODO: comentar algo
 *
 *        Version:  1.0
 *        Created:  14/05/16
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */
//

#include "Grasp.h"


Grasp::Grasp(int sizeRCL): Algorithm("Grasp"),sizeRCL(sizeRCL) {

}

Solution Grasp::solve(Problem problem, int size) {
    Solution solution(size);
    auto center = getCenter(problem);

    int remain = size;
    while (--remain >= 0) {
        Point point = getOneOfFarthestPoints(center, problem, sizeRCL);

        solution.insert(point);
        problem.erase(point);
        center = getCenter(solution);
    }
    return solution;
}