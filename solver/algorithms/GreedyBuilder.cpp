/*
 * =====================================================================================
 *
 *       Filename:  GreedyBuilder.cpp.h
 *
 *    Description:  Implementation
 *
 *        Version:  1.0
 *        Created:  9/05/16
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */
//

#include "GreedyBuilder.h"


GreedyBuilder::GreedyBuilder(void) : Algorithm("Greedy_Builder") {

}


Solution GreedyBuilder::solve(Problem problem, int size) {
    Solution solution(size);
    auto center = getCenter(problem);

    int remain = size;
    while (--remain >= 0) {
        Point point = getFarthestPoint(center, problem);
        solution.insert(point);
        problem.erase(point);
        center = getCenter(solution);
    }
    return solution;
}

