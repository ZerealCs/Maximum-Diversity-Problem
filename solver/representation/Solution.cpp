/*
 * =====================================================================================
 *
 *       Filename:  Solution.cpp.h
 *
 *    Description:  TODO: comentar algo
 *
 *        Version:  1.0
 *        Created:  9/05/16
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */
//

#include <iostream>
#include "Solution.h"

Solution::Solution(void): unordered_set() {

}

Solution::Solution(int size): unordered_set(size) {

}


double Solution::cost(void) {
    Solution aux = *this;
    double totalCost = 0.0;

    for (auto point : *this) {
        aux.erase(point);
        for (auto point2 : aux) {
            totalCost += point.distance(point2);
        }
    }
    return totalCost;
}



