/*
 * =====================================================================================
 *
 *       Filename:  Point.h
 *
 *    Description:  TODO: comentar algo
 *
 *        Version:  1.0
 *        Created:  8/05/16
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */


#ifndef MAXIMUM_DIVERSITY_PROBLEM_POINT_H
#define MAXIMUM_DIVERSITY_PROBLEM_POINT_H

#include <vector>
#include <istream>
#include <cmath>
#include <functional>
#include <iterator>

using namespace std;

typedef double Data;

// TODO: Number V => Point<V>
class Point : public vector<Data> {
public:

    /**
     * Generate a empty point 0-fixed vector
     */
    Point(void);

    /**
     * Generate a point k-fixed vector to 0 values
     */
    Point(int k);

    /**
     * Make a vector from a istream object using a fixed size
     */
    Point(istream & is, int fixedSize);

    /**
     * Euclidean distance between points
     */
    Data distance(Point point);

    /**
     * Map over each component of point
     */
    //void mapByComponent(function<void(Data&)> func);

    /**
     * Sum Operator
     */
    Point operator+(Point &);

    /**
     * Sum and assign Operator
     */
    Point operator+=(Point &);

    friend bool operator==(const Point &, const Point &);
    friend bool operator!=(const Point &, const Point &);
};

// TODO: Esto no esta medido hay que ver cual mejora
namespace std
{
    template<>
    struct hash<Point> {
        size_t operator()(const Point &pt) const {
            size_t initial = 0;
            for (Data data : pt) {
                initial ^= std::hash<int>()((int)data);
            }
            return initial;
        }
    };
}

template <typename T>
std::ostream& operator<< (std::ostream& out, const std::vector<T>& v) {
    if ( !v.empty() ) {
        out << '[';
        std::copy (v.begin(), v.end(), std::ostream_iterator<T>(out, ", "));
        out << "]";
    }
    return out;
}

#endif //MAXIMUM_DIVERSITY_PROBLEM_POINT_H
