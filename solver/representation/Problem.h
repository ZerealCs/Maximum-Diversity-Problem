/*
 * =====================================================================================
 *
 *       Filename:  Problem.h.h
 *
 *    Description:  TODO: comentar algo
 *
 *        Version:  1.0
 *        Created:  8/05/16
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */

#ifndef MAXIMUM_DIVERSITY_PROBLEM_PROBLEM_H
#define MAXIMUM_DIVERSITY_PROBLEM_PROBLEM_H

#include <iostream>
#include <functional>
#include <set>
#include <unordered_set>

#include "Point.h"
#include "Solution.h"

using namespace std;

/**
 * This class represents problem that is a list of point in n-dimensions
 */
class Problem : public unordered_set<Point> {
private:
    int kSize;
public:

    Problem();

    /**
     * Reads a problem from a input istream like a file.
     */
    Problem(istream & is);

    /**
     * Get k-Size of Point into set
     */
    int getKSize() const {
        return kSize;
    }


    /**
     * Generate a "random" solution(take n point from problem)
     */
    Solution getSolution(int n);

};

template <typename T>
std::ostream& operator<< (std::ostream& out, const std::unordered_set<T>& s) {
    // out << "Unordered Set: \n";
    if ( !s.empty() ) {
        out << "{ ";
        std::copy (s.begin(), s.end(), std::ostream_iterator<T>(out, " "));
        out << "}";
    }
    return out;
}

#endif //MAXIMUM_DIVERSITY_PROBLEM_PROBLEM_H
