/*
 * =====================================================================================
 *
 *       Filename:  Solution.h.h
 *
 *    Description:  TODO: comentar algo
 *
 *        Version:  1.0
 *        Created:  9/05/16
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */
//

#ifndef MAXIMUM_DIVERSITY_PROBLEM_SOLUTION_H
#define MAXIMUM_DIVERSITY_PROBLEM_SOLUTION_H

#include <vector>
#include <functional>
#include <unordered_set>
#include "Point.h"

using namespace std;

/**
 * Represents a vector with several points
 */
class Solution : public unordered_set<Point> {

public:
    Solution(void);

    Solution(int size);

    double cost(void);
};


#endif //MAXIMUM_DIVERSITY_PROBLEM_SOLUTION_H
