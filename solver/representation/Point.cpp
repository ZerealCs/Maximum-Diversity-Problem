/*
 * =====================================================================================
 *
 *       Filename:  Point.cpp
 *
 *    Description:  TODO: comentar algo
 *
 *        Version:  1.0
 *        Created:  8/05/16
 *       Revision:  none
 *       Compiler:  clang
 *
 *         Author:  Eleazar Díaz Delgado(eleazardzdo at gmail dot com),
 *     University:  ULL
 *
 * =====================================================================================
 */

#include <iostream>
#include "Point.h"

Point::Point(void): vector() {

}

Point::Point(int k): vector(k, 0) {

}

Point::Point(istream &is, int fixedSize) : vector(fixedSize) {
    for (int i = 0; i < fixedSize; ++i) {
        is >> operator[](i);
    }
}


Data Point::distance(Point point) {
    if (point.size() != size()) {
        throw -1;
    }
    Data result = 0.0;
    for (int i = 0; i < size(); ++i) {
        result += pow(operator[](i) - point[i], 2);
    }
    return sqrt(result);
}

/*
void Point::mapByComponent(function<void (Data&)> fun) {
    for (int i = 0; i < size(); ++i) {
        fun(operator[](i));
    }
}
*/

Point Point::operator+(Point & p1) {
    for (int i = 0; i < size(); ++i) {
        operator[](i) += p1[i];
    }
    return *this;
}


Point Point::operator+=(Point & p) {
    for (int i = 0; i < size(); ++i) {
        operator[](i) += p[i];
    }
    return *this;
}

bool operator== (const Point &p1, const Point &p2)
{
    bool all = true;
    for (int i = 0; i < p1.size(); ++i) {
        all &= p2[i] == p1[i];
    }
    return all;
}

bool operator!= (const Point &c1, const Point &c2)
{
    return !(c1== c2);
}


